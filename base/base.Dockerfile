FROM nvidia/cuda:11.1.1-cudnn8-runtime-ubuntu20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get clean && \
    apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install --no-install-recommends -y locales && \
    apt-get install -y python3-pip screen psmisc libgl1-mesa-glx libgtk2.0-dev libpcl-dev pybind11-dev && \
    rm -rf /var/lib/apt/lists/* && \
    locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Install locales to prevent errors
RUN apt-get clean && \
    apt-get update && \
    apt-get install -y git wget python3 nlohmann-json3-dev libtinyxml2-dev libtinyxml-dev libssl-dev libspdlog-dev libeigen3-dev unzip gnupg2 lsb-release curl libyaml-cpp-dev libpcl-dev build-essential && \
    sh -c 'echo "deb [arch=amd64,arm64] http://repo.ros2.org/ubuntu/main `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list' && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
    apt-get update && \
    apt-get install -y ros-foxy-ros-base ros-foxy-ament-cmake-python ros-foxy-cv-bridge ros-foxy-pcl-conversions ros-foxy-ompl  && \
    apt-get install -y python3-colcon-common-extensions && \
    apt-get install -y libopencv-dev python3-opencv && \
    apt-get install -y supervisor && \
    rm -rf /var/lib/apt/lists/*

# Bullet
RUN wget https://github.com/bulletphysics/bullet3/archive/refs/tags/3.17.zip && \
    unzip 3.17.zip && \
    cd bullet3-3.17/build3 && \
    cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j3 && \
    make install


# Install detectron with dependencies
RUN pip3 install --upgrade pip \
    && pip3 install setuptools==50.3.0 \
    && wget -l 100 ftp://10.3.14.35/dockers/python_packages/torchvision-0.8.2%2Bcu110-cp38-cp38-linux_x86_64.whl \
    && wget -l 100 ftp://10.3.14.35/dockers/python_packages/torch-1.7.1%2Bcu110-cp38-cp38-linux_x86_64.whl \
    && wget -l 100 ftp://10.3.14.35/dockers/python_packages/torchaudio-0.7.2-cp38-cp38-linux_x86_64.whl \
    && pip install --default-timeout=100 torchvision-0.8.2+cu110-cp38-cp38-linux_x86_64.whl torch-1.7.1+cu110-cp38-cp38-linux_x86_64.whl torchaudio-0.7.2-cp38-cp38-linux_x86_64.whl \
    && pip install orjson \
    && pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu110/torch1.7/index.html \
    && pip install opencv-python \
    && pip install pybind11 \
    && rm -rf /root/.cache/

RUN rm -rf ~/detectron2_weights && \
    mkdir -p ~/detectron2_weights && cd ~/detectron2_weights && \
    wget -l 100 ftp://10.3.14.35/ftp_models/ml_vision_blender_dir/class_names.yaml && \
    wget -l 100 ftp://10.3.14.35/ftp_models/ml_vision_blender_dir/r101_aws_pointrend_blender.pth && \
    wget -l 100 ftp://10.3.14.35/ftp_models/ml_vision_blender_dir/pointrend_R101.yaml

ADD setup_repository.sh /setup_repository.sh

RUN ./setup_repository.sh movement/ commons/ core/ vision/

COPY base/bootstrap.sh /bootstrap.sh
RUN chmod 755 /bootstrap.sh
COPY base/supervisord.conf /etc/supervisord.conf
RUN chmod 755 /etc/supervisord.conf

# ENTRYPOINT ["/usr/bin/supervisord"]
CMD ["tail", "-f", "/dev/null"]