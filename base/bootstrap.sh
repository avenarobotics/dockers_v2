#!/bin/bash
set -e


# for DRM in /dev/dri/card*; do
#   if /opt/VirtualGL/bin/eglinfo "$DRM"; then
#     export VGL_DISPLAY="$DRM"
#     break
#   fi
# done
source /opt/ros/foxy/setup.bash
source /root/ros2_ws/install/setup.bash
ros2 launch avena_bringup docker.launch.py

echo "Session Running. Press [Return] to exit."
read
