#!/bin/bash

mkdir -p ~/ros2_ws/src 
cd ~/ros2_ws/src

git clone --depth 1 -b dockers_v2 https://gitlab.com/avenarobotics/avena_system.git
cd avena_system


active_domains="$@"

echo $active_domains

for directory in $(ls -d */)
do
    if [[ ! "${active_domains[*]}" =~ "${directory}" ]]
    then
        echo "IGNORING $directory domain"
        cd "${directory}"
        touch COLCON_IGNORE
        cd ..
    fi
done