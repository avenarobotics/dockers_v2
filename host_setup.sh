#!/bin/bash
echo "[sudo] password for user $USER:"
read -s pass

if [[ "$1" = "build" ]]; 
then
    echo "######################################################"
    echo "####          INSTALLING HOST PACKAGES            ####"
    echo "######################################################"
    sleep 1
    rm -rf ~/ros2_ws
    mkdir -p ~/ros2_ws
    echo "$pass" | sudo -S apt-get update
    sudo apt install -y build-essential ros-foxy-ros-base ros-foxy-ament-cmake-python ros-foxy-ompl ros-foxy-rqt*

    # colcon 
    sudo -S sh -c 'echo "deb [arch=amd64,arm64] http://repo.ros2.org/ubuntu/main `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install -y python3-colcon-common-extensions

    # host dependencies
    sudo apt-get install -y nlohmann-json3-dev libyaml-cpp-dev
    bash ./setup_repository.sh commons/ drivers/ ui/
    cd ~/ros2_ws
    source /opt/ros/foxy/setup.bash
    colcon build --parallel-workers 8
    cd ~/dockers_v2
elif [[ "$1" = "up" ]]; 
then
    echo "Bringing up avena system environment..."
elif [[ "$1" = "pull" ]];
then
    echo "Not implemented yet"
else
    echo "Supported commands are build, pull, up, you provided "$1""
    exit 0
fi

echo "######################################################"
echo "####             SETTING UP DOCKERS               ####"
echo "######################################################"
sleep 1

# This section is responsible for setting up all dockers for the avena system
# ./startup/start_environment.sh "$1"

if [[ "$1" = "build" || "$1" = "pull" ]]; 
then
    docker-compose "$1" base

elif [[ "$1" = "up" ]]; 
then
    echo "Bringing up avena system environment..."
else
    echo "Supported script arguments are build, pull, up, you provided "$1""
    exit 0
fi

# build ros2 packages on the CORE docker
docker-compose up --detach --no-build base
sleep 2


if [[ "$1" = "build" || "$1" = "pull" ]]; 
then
    docker exec -it base_container bash -c "source /opt/ros/foxy/setup.bash ; cd ~/ros2_ws/ ; colcon build ; echo 'source /opt/ros/foxy/setup.bash' >> ~/.bashrc ; echo 'source ~/ros2_ws/install/setup.bash' >> ~/.bashrc "
elif [[ "$1" = "up" ]]; 
then
    docker exec -it base_container bash -c "cd ~/ros2_ws/ ; colcon build "
else
    exit 0
fi

source /opt/ros/foxy/setup.bash
source ~/ros2_ws/install/setup.bash
